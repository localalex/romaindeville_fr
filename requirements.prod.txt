#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile requirements.prod.in
#
babel==2.9.1
    # via -r requirements.prod.in
coloredlogs==15.0
    # via -r requirements.prod.in
humanfriendly==9.1
    # via coloredlogs
jinja2==3.0.1
    # via -r requirements.prod.in
markdown==3.3.4
    # via -r requirements.prod.in
markupsafe==2.0.1
    # via jinja2
ordered-set==4.0.2
    # via pylatex
pylatex==1.4.1
    # via -r requirements.prod.in
python-dateutil==2.8.1
    # via -r requirements.prod.in
pytz==2021.1
    # via babel
pyyaml==5.4.1
    # via -r requirements.prod.in
six==1.16.0
    # via python-dateutil
